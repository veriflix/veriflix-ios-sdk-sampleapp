//  Copyright © 2020 Company. All rights reserved.
//

import UIKit
import VeriflixSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setupVeriflixSDK()
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }
}

private extension AppDelegate {

    func setupVeriflixSDK() {
        let stdOutLogger = VeriflixStdOutLogger()
        
        Veriflix.configure(
            config: .init(
                consumerId: "lng_sdk",
                sdkKey: "",
                environment: .qa, // 
                loggers: [stdOutLogger],
                shouldShowUploadFailedAlerts: true
//                videoEditorSdkLicenseFileUrl: <Add url to VESDK license file>
            )
        )
    }
}
