//  Copyright © 2020 Lookapp. All rights reserved.
//

import UIKit
import JGProgressHUD

extension UIViewController {
    func showHud(_ show: Bool, style: JGProgressHUDStyle = .light) {
        if show {
            let hud = JGProgressHUD(style: style)
            hud.show(in: self.view)
        } else {
            JGProgressHUD.allProgressHUDs(in: self.view).forEach {
                $0.dismiss()
            }
        }
    }
}
