//  Copyright © 2020 Lookapp. All rights reserved.
//

import UIKit
import MobileCoreServices
import CoreLocation
import Photos

final class ImagePickerAdapter: NSObject {
    override init() {
        super.init()
        selfLink = self
    }

    var onSelectAsset: ((Asset) -> Void)?
    var onError: ((Error) -> Void)?
    var onCancel: (() -> Void)?
    var onShowLoading:((Bool) -> Void)?
    var shouldCopyToDocuments: Bool = true
    var selfLink: ImagePickerAdapter?
}

extension ImagePickerAdapter {
    enum FileType {
        case movie
        case image
    }

    struct Asset {
        let type: FileType
        let fileUrl: URL
        let image: UIImage?
        let location: CLLocation?
        let creationDate: Date?
    }
}

extension ImagePickerAdapter: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let asset = makeAsset(from: info) else {
            onError?(NSError(message: "Failed to pick media"))
            return
        }
        
        if !shouldCopyToDocuments {
            onSelectAsset?(asset)
            selfLink = nil
        } else {
            guard let documentsDir = documentsDir() else {
                onError?(NSError(message: "No documents dir"))
                return
            }
            onShowLoading?(true)
            asset.save(to: documentsDir) { result in
                self.onShowLoading?(false)
                switch result {
                case .success(let newAsset):
                    self.onSelectAsset?(newAsset)
                case .failure(let error):
                    self.onError?(error)
                }
                self.selfLink = nil
            }
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        onCancel?()
        selfLink = nil
    }

}

private extension ImagePickerAdapter {
    func makeAsset(from info: [UIImagePickerController.InfoKey : Any]) -> Asset? {
        guard let mediaType = info[.mediaType] as? String else {
            return nil
        }

        var type: FileType
        if mediaType == kUTTypeImage as String {
            type = .image
        } else if mediaType == kUTTypeMovie as String {
            type = .movie
        } else {
            assertionFailure()
            return nil
        }

        var fileUrl: URL
        var image: UIImage?
        var location: CLLocation?
        var creationDate: Date?

        switch type {
        case .image:
            guard let imageUrl = info[.imageURL] as? URL else {
                return nil
            }
            guard let originalImage = info[.originalImage] as? UIImage else {
                return nil
            }
            image = originalImage
            fileUrl = imageUrl
        case .movie:
            guard let mediaUrl = info[.mediaURL] as? URL else {
                return nil
            }
            fileUrl = mediaUrl
        }

        if let phAsset = info[.phAsset] as? PHAsset {
            location = phAsset.location
            creationDate = phAsset.creationDate
        }

        return Asset(type: type, fileUrl: fileUrl, image: image, location: location, creationDate: creationDate)
    }

    func documentsDir() -> URL? {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
    }
}

extension ImagePickerAdapter.Asset {
    func save(to url: URL, completion: @escaping ((Result<ImagePickerAdapter.Asset, Error>) -> Void)) {

        let fileName = fileUrl.lastPathComponent
        let resultUrl = url.appendingPathComponent(fileName)
        let sourceUrl = fileUrl

        DispatchQueue.global(qos: .userInitiated).async {
            do {
                try FileManager.default.copyItem(at: sourceUrl, to: resultUrl)
            }
            catch {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }

            DispatchQueue.main.async {
                let newAsset = ImagePickerAdapter.Asset(
                    type: self.type,
                    fileUrl: resultUrl,
                    image: self.image,
                    location: self.location,
                    creationDate: Date()
                )
                completion(.success(newAsset))
            }
        }
    }
}
