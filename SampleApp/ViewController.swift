//  Copyright © 2020 Company. All rights reserved.
//

import UIKit
import VeriflixSDK
import CoreLocation
import CoreServices
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var openRecorderButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        update()
    }
}

// MARK:- Private types
extension ViewController {
    enum MenuItem {
        case photoLibrary
        case recordVideo
        case takePhoto
        case sendMessage
    }
}

// MARK:- Private methods
private extension ViewController {
    @IBAction func onSendVideo() {
        checkIfSdkAvailable {
            self.authenticateIfNeeded {
                self.sendVideo()
            }
        }
    }
    
    func checkIfSdkAvailable(successfulCompletion: @escaping (() -> Void)) {
        Veriflix.sdk.availabilityService.isSdkAvailable { result in
            switch result {
            case .failure(let error):
                self.show(error: error)
            case .success(let available):
                if available {
                    successfulCompletion()
                } else {
                    self.show(message: NSLocalizedString("Video recording service is not available, please try again later.", comment: ""))
                }
            }
        }
    }
    
    func authenticateIfNeeded(successfulCompletion: @escaping (() -> Void)) {
        if Veriflix.sdk.authService.isAuthenticated {
            successfulCompletion()
        } else {
            self.loginOrSignup {
                successfulCompletion()
            }
        }
    }

    func loginOrSignup(completion: @escaping ((Result<Void, Error>) -> Void)) {
        let email = "sampleapp@veriflix.ai"
        let firstName = "John"
        let lastName = "Smith"

        Veriflix.sdk.authService.login(email: email, firstName: firstName, lastName: lastName) { result in
            switch result {
            case .failure:
                Veriflix.sdk.authService.signUp(email: email, firstName: firstName, lastName: lastName) { _ in
                    switch result {
                    case .failure(let error):
                        completion(.failure(error))
                    case .success:
                        self.update()
                        completion(.success(()))
                    }
                }
            case .success:
                completion(.success(()))
            }
        }
    }

    func sendVideo() {
        showMenu { item in
            switch item {
            case .photoLibrary:
                self.showMediaPicker { pickerAsset in
                    let creationDate: Date = pickerAsset.creationDate ?? Date()
                    let asset = RecorderLibraryAsset(
                        fileUrl: pickerAsset.fileUrl,
                        creationDate: creationDate,
                        image: pickerAsset.image
                    )

                    self.openRecorder(mode: pickerAsset.type.toRecorderMode(), source: .library(assets: [asset]))
                }
            case .recordVideo:
                Veriflix.sdk.permissionsService.checkOrRequest(permissions: [.camera, .microphone]) { allGranted in
                    if allGranted {
                        self.openRecorder(mode: .video, source: .camera)
                    }
                }
            case .takePhoto:
                Veriflix.sdk.permissionsService.checkOrRequest(permissions: [.camera]) { allGranted in
                    if allGranted {
                        self.openRecorder(mode: .photo, source: .camera)
                    }
                }
            case .sendMessage:
                self.sendMessage()
            }
        }
    }

    func loginOrSignup(successfulCompletion: @escaping (() -> Void)) {
        infoLabel.text = "Logging in..."
        
//        successfulCompletion()
//        return
        
        self.showHud(true)

        loginOrSignup { result in
            self.showHud(false)
            self.update()
            switch result {
            case .failure(let error):
                self.show(error: error)
            case .success:
                successfulCompletion()
            }
        }
    }

    func logout() {
        infoLabel.text = "Logging out..."
        Veriflix.sdk.authService.logout { _ in
            self.update()
        }
    }
    
    func update() {
        if Veriflix.sdk.authService.isAuthenticated {
            if let user = Veriflix.sdk.stateService.currentUserProfile {
                infoLabel.text = "Logged in as \(user.firstName) \(user.lastName) (\(user.username))"
            } else {
                infoLabel.text = "Logged in"
            }
        } else {
            infoLabel.text = "Not logged in"
        }
    }

    func showMenu(completion: @escaping ((MenuItem) -> Void)) {
        let alertController = UIAlertController(title: NSLocalizedString("Alert us", comment: ""), message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(
            UIAlertAction(
                title: NSLocalizedString("Record video", comment: ""),
                style: .default
            ) { _ in
                completion(.recordVideo)
            }
        )
        
        alertController.addAction(
            UIAlertAction(
                title: NSLocalizedString("Take photo", comment: ""),
                style: .default
            ) { _ in
                completion(.takePhoto)
            }
        )
        
        alertController.addAction(
            UIAlertAction(
                title: NSLocalizedString("Choose video/photo from library", comment: ""),
                style: .default
            ) { _ in
                completion(.photoLibrary)
            }
        )
        
        alertController.addAction(
            UIAlertAction(
                title: NSLocalizedString("Send message", comment: ""),
                style: .default
            ) { _ in
                completion(.sendMessage)
            }
        )
        
        alertController.addAction(
            UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel)
        )

        present(alertController, animated: true)
    }

    func showMediaPicker(completion: @escaping ((_ asset: ImagePickerAdapter.Asset) -> Void)) {
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
            return
        }

        let vc = UIImagePickerController()

        let adapter = ImagePickerAdapter()
        adapter.onSelectAsset = { [weak self] asset in
            self?.dismiss(animated: true)
            completion(asset)
        }
        adapter.onCancel = { [weak self] in
            self?.dismiss(animated: true)
        }
        adapter.onShowLoading = { [weak self] loading in
            self?.showHud(loading)
        }
        adapter.onError = { [weak self] error in
            self?.show(error: error)
        }

        vc.delegate = adapter
        vc.allowsEditing = false
        vc.sourceType = .photoLibrary
        vc.mediaTypes = [kUTTypeImage, kUTTypeMovie].map { $0 as String }
        vc.videoExportPreset = AVAssetExportPreset1920x1080
        present(vc, animated: true)
    }

    func openRecorder(mode: RecorderMode, source: RecorderSource) {
        let theme = VeriflixTheme.SITheme(
            fonts: .init(
                font: UIFont(name: "MonumentExtended", size: UIFont.systemFontSize)!,
                additionalBoldFont: UIFont(name: "Roboto-Bold", size: UIFont.systemFontSize)!,
                additionalMediumFont: UIFont(name: "Roboto-Medium", size: UIFont.systemFontSize)!,
                additionalRegularFont: UIFont(name: "Roboto-Regular", size: UIFont.systemFontSize)!
            )
        )

        let config = RecorderConfiguration(
            mode: mode,
            source: source,
            includeDescriptionField: false,
            theme: theme,
            location: Veriflix.sdk.locationManager.lastKnownLocation,
            editorSettings: [.text, .trim],
            hasAutoFill: true,
            supportsPortraitMode: true,
            isEditingPortrait: true
        )

        let output = RecorderOutput()

        let vc = VeriflixRecorderViewController(configuration: config, output: output)

        output.done = { [weak vc] in
            vc?.dismiss(animated: true)
        }
        
        vc.modalPresentationStyle = .fullScreen

        present(vc, animated: true)
    }

    func sendMessage() {
        print("TODO: implement sendMessage")
    }
}

extension ImagePickerAdapter.FileType {
    func toRecorderMode() -> RecorderMode {
        switch self {
        case .image:
            return .photo
        case .movie:
            return .video
        }
    }
}
